package nl.codefoundry

import com.google.gson.GsonBuilder
import io.github.serpro69.kfaker.Faker
import io.github.serpro69.kfaker.fakerConfig

val dutchFaker = Faker(fakerConfig { locale = "nl-NL" })
val spanishFaker = Faker(fakerConfig { locale = "es" })

fun main() {
    val sender = Addressee(
        firstName = dutchFaker.name.firstName(),
        lastName = dutchFaker.name.lastName(),
        email = dutchFaker.internet.safeEmail(),
        phone = dutchFaker.phoneNumber.phoneNumber(),
        address = Address(
            street = dutchFaker.address.streetName(),
            houseNumber = dutchFaker.address.buildingNumber(),
            city = dutchFaker.address.city(),
            country = "NL"
        )
    )

    val receiver = Addressee(
        firstName = spanishFaker.name.firstName(),
        lastName = spanishFaker.name.lastName(),
        email = spanishFaker.internet.email(),
        phone = spanishFaker.phoneNumber.phoneNumber(),
        address = Address(
            street = spanishFaker.address.streetName(),
            houseNumber = spanishFaker.address.buildingNumber(),
            city = spanishFaker.address.city(),
            country = "ES"
        )
    )

    val request = TransportRequest(
        sender = sender,
        receiver = receiver,
        trackTrace = dutchFaker.code.asin(),
        comments = dutchFaker.yoda.quotes()
    )

    println(GsonBuilder().setPrettyPrinting().create().toJson(request))
}