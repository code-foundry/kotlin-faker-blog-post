package nl.codefoundry

import io.github.serpro69.kfaker.Faker

fun main() {
    val faker = Faker()
    val name = faker.name.firstName()
    val phoneNumber = faker.phoneNumber.phoneNumber()
    val email = faker.internet.safeEmail()
    val city = faker.address.city()
    val streetName = faker.address.streetName()
    val houseNumber = faker.address.buildingNumber()

    println("Hello, I am $name living in $city, $streetName $houseNumber." +
            "\nMy email is $email and my phone number is $phoneNumber.")
}
