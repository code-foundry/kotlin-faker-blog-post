package nl.codefoundry

data class TransportRequest(
    val sender: Addressee,
    val receiver: Addressee,
    val trackTrace: String,
    val comments: String? = null
)

//TransportRequest(
//    sender=Addressee(firstName=Shardée, lastName=Isenia, email=carucha.hilgenga.jr@hotmail.test, phone=0478151095,
//        address=Address(street=Robbersstraat, houseNumber=111a, city=Oost Schollhout, country=NL)),
//    receiver=Addressee(firstName=Elvira, lastName=Olivárez, email=sra.javier.mateo.casárez@hotmail.com, phone=930391577,
//        address=Address(street=Parcela Miguel Sisneros, houseNumber= 5, city=San Sebastián, country=ES)),
//    trackTrace=B00069TDVW,
//    comments=Adventure. Excitement. A Jedi craves not these things.)