package nl.codefoundry

data class Addressee(
    val firstName: String,
    val lastName: String,
    val email: String,
    val phone: String,
    val address: Address
)
